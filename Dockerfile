# Load the base pytorch image
# FROM pytorch/pytorch:2.0.1-cuda11.7-cudnn8-devel
# FROM pytorch/pytorch:2.1.2-cuda11.8-cudnn8-devel
# FROM bitnami/pytorch:latest
FROM nvidia/cuda:11.8.0-runtime-ubi8

# Local arguments and envs
# dates/times/numbers
ENV LANG C.UTF-8 
ENV LC_ALL C.UTF-8
ENV PIP_ROOT_USER_ACTION=ignore
ARG DEBIAN_FRONTEND=noninteractive

# Update and install packages
RUN apt-get -y update && apt-get -y upgrade 
RUN apt-get -y install \
    build-essential \
    wget \
    curl \
    git \
    make \
    gcc \
    graphviz \
    sudo \
    h5utils \
    vim 

COPY requirements.txt requirements.txt

RUN python -m pip install --no-cache-dir --upgrade pip && \
    python -m pip install --no-cache-dir --upgrade -r requirements.txt

RUN python -m pip install torch==2.1.1 torchvision==0.16.1 --index-url https://download.pytorch.org/whl/cu118
RUN python -m pip install --no-cache-dir torch_geometric
# No pyg_lib
RUN python -m pip install --no-cache-dir torch_scatter torch_sparse torch_cluster torch_spline_conv -f https://data.pyg.org/whl/torch-2.1.0+cu118.html

